# Space Map #
App to display the list of meeting rooms and book if there are slots available

### Screenshots ###
![Alt text](art/Screenshot_1.png "Screenshots")

### My Thought process ###
I time capped the assignment for 3-4 hours. I decided to go with MVVM and to focus on the repository for supporting the offline data. I wanted to extract things to use-cases but couldn't do it on time. Also the booking room part, I wasn't sure on how to support offline mode for it - like decrement offline count each time of booking? wasn't sure of that.

I tried my best to extract logic to testable units as much as possible - for mapper, repository and viewmodel. For View, I wanted to do Snapshot testing or Espresso but couldn't do it on time. Overall it was fun creating this from scratch and within the timeframe. I've marked few TODOs in the code that I couldn't finish. Please check my commits for the layer construction from ground up

### My Personal challenges ###
For the past 2.5 years working for FinTech product, I've lost my touch with retrofit and room database. For the product that I work with, we auto generate the network client and strictly no database for data protection. So getting my hands dirty with offline app and database was a refreshing challenge and I hope I made some justice for it.2

### Things inside ###
* MVVM architecture
* Repository abstract data sources
* Separation of concerns
* Unit testing
* Service locator module test
* Proper themes and resource handling
* App is bit more accessible
* Unit tests with good code coverage for repository, viewModel and mapper

### Some TODOs ###

* Offline logic for booking a room
* Network check service to check for internet connection
* Better empty state
* Different theme for button view for disabled entity
* Glide default color for loading
* remove synthetic binding and use view binding
* Swipe to refresh for the list
* Move button enable disable logic out of adapter to mapper
* clean dimes resources
* Different callstate for each call to avoid unused implementation
* UI test - snapshot or espresso

### Stack ###

* MVVM
* Kotlin
* Architecture components
* Room database
* Koin
* Retrofit
* Coroutines
* Glide
* Mockito
* Shot