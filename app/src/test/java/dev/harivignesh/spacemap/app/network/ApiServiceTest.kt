package dev.harivignesh.spacemap.app.network

import dev.harivignesh.spacemap.app.room.model.Room
import dev.harivignesh.spacemap.app.room.model.RoomResponse
import dev.harivignesh.spacemap.app.room.network.ApiService
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

/**
 * Created by Hari B.V on 26/09/2021.
 * API service test
 */
@RunWith(MockitoJUnitRunner::class)
class ApiServiceTest {

    private val room = Room("randomName", 5, "url")
    private val fakeRoomResponse = RoomResponse(rooms = listOf(room))
    @Mock private lateinit var service: ApiService

    @Test
    fun test_api_service_get_room() {
        //TODO: improve testing network calls with mock server
        runBlocking {
            `when`(service.getRooms()).then { fakeRoomResponse }

            assertEquals(service.getRooms().rooms.first().name, "randomName")
            assertEquals(service.getRooms().rooms.first().spots, 5)
            assertEquals(service.getRooms().rooms.first().thumbnail, "url")

        }
    }
}