package dev.harivignesh.spacemap.app.repository

import dev.harivignesh.spacemap.app.room.model.Room
import dev.harivignesh.spacemap.app.room.repository.mapToEntity
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

/**
 * Created by Hari on 26/09/2021.
 * Unit test for [RoomMapper]
 */
@RunWith(MockitoJUnitRunner::class)
class RoomMapperTest {

    @Test
    fun map_room_to_roomEntity() {
        val room = Room("randomName", 5, "url")
        val result = room.mapToEntity(1)
        assertEquals(result.id, 1)
        assertEquals(result.name, room.name)
        assertEquals(result.spots, room.spots)
        assertEquals(result.thumbnail, room.thumbnail)
    }
}