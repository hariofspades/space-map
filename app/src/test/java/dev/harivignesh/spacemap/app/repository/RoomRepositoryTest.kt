package dev.harivignesh.spacemap.app.repository

import dev.harivignesh.spacemap.app.room.cache.RoomDao
import dev.harivignesh.spacemap.app.room.model.BookResponse
import dev.harivignesh.spacemap.app.room.model.Room
import dev.harivignesh.spacemap.app.room.model.RoomResponse
import dev.harivignesh.spacemap.app.room.network.ApiService
import dev.harivignesh.spacemap.app.room.repository.RoomRepository
import dev.harivignesh.spacemap.app.room.repository.RoomRepositoryImpl
import dev.harivignesh.spacemap.app.room.repository.mapToEntity
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.lang.Exception
import kotlin.test.assertEquals

/**
 * Created by Hari on 26/09/2021.
 * Unit test for [RoomRepository]
 */
@RunWith(MockitoJUnitRunner::class)
class RoomRepositoryTest {

    private val room = Room("randomName", 5, "url")
    private val fakeRoomResponse = RoomResponse(rooms = listOf(room))
    private val fakeRoomEntity = listOf(room).mapIndexed { index, room -> room.mapToEntity(index) }
    private val fakeBookResponse = BookResponse(true)

    @Mock private lateinit var service: ApiService
    @Mock private lateinit var dao: RoomDao

    @Test
    fun get_rooms_returns_data_on_network_writes_and_shows_from_db() {
        runBlocking {
            val repository: RoomRepository = RoomRepositoryImpl(service, dao)
            `when`(service.getRooms()).then { fakeRoomResponse }
            `when`(dao.getRooms()).then { fakeRoomEntity }

            val list = repository.getRooms()

            verify(dao, times(1)).insertAll(fakeRoomEntity)
            verify(dao, times(1)).getRooms()

            assertEquals(list.first().name, room.name)
            assertEquals(list.first().spots, room.spots)
            assertEquals(list.first().thumbnail, room.thumbnail)
        }
    }

    @Test
    fun get_rooms_returns_data_from_db_when_offline() {
        runBlocking {
            val repository: RoomRepository = RoomRepositoryImpl(service, dao)
            `when`(service.getRooms()).then { throw Exception("network error") }
            `when`(dao.getRooms()).then { fakeRoomEntity }

            val list = repository.getRooms()

            verify(dao, times(0)).insertAll(fakeRoomEntity)
            verify(dao, times(1)).getRooms()

            assertEquals(list.first().name, room.name)
            assertEquals(list.first().spots, room.spots)
            assertEquals(list.first().thumbnail, room.thumbnail)
        }
    }

    @Test
    fun book_room_returns_data_from_api() {
        runBlocking {
            val repository: RoomRepository = RoomRepositoryImpl(service, dao)
            `when`(service.bookRoom()).then { fakeBookResponse }
            val result = repository.bookRoom()
            assertEquals(result, true)
        }
    }

}