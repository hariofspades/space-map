package dev.harivignesh.spacemap.app.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import dev.harivignesh.spacemap.app.room.model.CallState
import dev.harivignesh.spacemap.app.room.model.Room
import dev.harivignesh.spacemap.app.room.model.RoomEntity
import dev.harivignesh.spacemap.app.room.presentation.RoomViewModel
import dev.harivignesh.spacemap.app.room.repository.RoomRepository
import dev.harivignesh.spacemap.app.util.TestCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

/**
 * Created by Hari on 26/09/2021.
 * Unit test for [RoomViewModel]
 */
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RoomViewModelTest  {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var repository: RoomRepository

    @Mock
    private lateinit var roomListObserver: Observer<CallState<out List<RoomEntity>>>

    @Mock
    private lateinit var bookRoomObserver: Observer<CallState<out Boolean>>

    @Test
    fun on_initialising_viewModel_rooms_are_fetched_empty_state() {
        runBlockingTest {
            `when`(repository.getRooms()).then { emptyList<Room>() }
            val viewModel = RoomViewModel(repository)
            viewModel.roomListState.observeForever(roomListObserver)

            verify(repository).getRooms()
            assertEquals(CallState.Empty, viewModel.roomListState.value)

            viewModel.roomListState.removeObserver(roomListObserver)
        }
    }

    @Test
    fun on_initialising_viewModel_rooms_are_fetched_not_empty_state() {
        runBlockingTest {
            val roomList = listOf(RoomEntity( 1,"name", 10, "fake_url"))
            `when`(repository.getRooms()).then { roomList }
            val viewModel = RoomViewModel(repository)
            viewModel.roomListState.observeForever(roomListObserver)

            verify(repository).getRooms()
            val result = (viewModel.roomListState.value as CallState.Success).data
            assertEquals(roomList.first(), result.first())

            viewModel.roomListState.removeObserver(roomListObserver)
        }
    }

    @Test
    fun on_initialising_viewModel_rooms_are_fetched_not_error_state() {
        runBlockingTest {
            `when`(repository.getRooms()).then { throw Exception("failure") }
            val viewModel = RoomViewModel(repository)
            viewModel.roomListState.observeForever(roomListObserver)

            verify(repository).getRooms()
            val result = (viewModel.roomListState.value as CallState.Error).errorResponse
            assertEquals(result, "failure")

            viewModel.roomListState.removeObserver(roomListObserver)
        }
    }

    @Test
    fun on_room_booked_success_state() {
        runBlockingTest {
            `when`(repository.bookRoom()).then { true }
            val viewModel = RoomViewModel(repository)
            viewModel.bookRoom()
            viewModel.bookRoomState.observeForever(bookRoomObserver)

            verify(repository).bookRoom()
            val result = (viewModel.bookRoomState.value as CallState.Success).data
            assertEquals(result, true)

            viewModel.bookRoomState.removeObserver(bookRoomObserver)
        }
    }

    @Test
    fun on_room_booked_failure_state() {
        runBlockingTest {
            `when`(repository.bookRoom()).then { throw Exception("failure") }
            val viewModel = RoomViewModel(repository)
            viewModel.bookRoom()
            viewModel.bookRoomState.observeForever(bookRoomObserver)

            verify(repository).bookRoom()
            val result = (viewModel.bookRoomState.value as CallState.Error).errorResponse
            assertEquals(result, "failure")

            viewModel.bookRoomState.removeObserver(bookRoomObserver)
        }
    }
}