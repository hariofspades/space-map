package dev.harivignesh.spacemap.app

import dev.harivignesh.spacemap.app.room.appModule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.check.checkModules
import org.koin.test.mock.MockProviderRule
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Hari on 26/09/2021.
 * Koin module test
 */
@RunWith(MockitoJUnitRunner::class)
class AppModuleTest: AutoCloseKoinTest() {

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        Mockito.mock(clazz.java)
    }

    @Test
    fun checkAllModules() {
        startKoin { appModule }.checkModules()
    }
}