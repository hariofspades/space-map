package dev.harivignesh.spacemap.app.room.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Hari on 26/09/2021.
 * model for cache
 */
@Entity
data class RoomEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val name: String,
    val spots: Int,
    val thumbnail: String
)