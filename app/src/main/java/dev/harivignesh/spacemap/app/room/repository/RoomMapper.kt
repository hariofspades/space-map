package dev.harivignesh.spacemap.app.room.repository

import dev.harivignesh.spacemap.app.room.model.Room
import dev.harivignesh.spacemap.app.room.model.RoomEntity

/**
 * Created by Hari on 26/09/2021.
 * Mapper for room
 */
fun Room.mapToEntity(id: Int): RoomEntity = RoomEntity(
    id = id,
    name = this.name,
    spots = this.spots,
    thumbnail = this.thumbnail
)