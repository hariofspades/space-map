package dev.harivignesh.spacemap.app.room.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.harivignesh.spacemap.app.room.model.CallState
import dev.harivignesh.spacemap.app.room.model.Room
import dev.harivignesh.spacemap.app.room.model.RoomEntity
import dev.harivignesh.spacemap.app.room.repository.RoomRepository
import kotlinx.coroutines.launch

/**
 * Created by Hari on 26/09/2021.
 * View model for room
 */
class RoomViewModel(private val repository: RoomRepository) : ViewModel() {

    private val mutableRoomsState: MutableLiveData<CallState<out List<RoomEntity>>> = MutableLiveData()
    private val mutableBookRoomState: MutableLiveData<CallState<out Boolean>> = MutableLiveData()

    val roomListState: LiveData<CallState<out List<RoomEntity>>> = mutableRoomsState
    val bookRoomState: LiveData<CallState<out Boolean>> = mutableBookRoomState

    init {
        getRooms()
    }

    private fun getRooms() = viewModelScope.launch {
        try {
            mutableRoomsState.postValue(CallState.OnLoading)
            val rooms = repository.getRooms()
            val state = if (rooms.isEmpty()) CallState.Empty else CallState.Success(rooms)
            mutableRoomsState.postValue(state)
        } catch (exception: Exception) {
            mutableRoomsState.postValue(CallState.Error(exception.message))
        }
    }

    fun bookRoom() = viewModelScope.launch {
        try {
            val bookRoom = repository.bookRoom()
            mutableBookRoomState.postValue(CallState.Success(bookRoom))
        } catch (exception: Exception) {
            mutableBookRoomState.postValue(CallState.Error(exception.message))
        }
    }
}