package dev.harivignesh.spacemap.app.room.model

/**
 * Created by Hari on 26/09/2021.
 * Response model for booking a room
 */
data class BookResponse(val success: Boolean)