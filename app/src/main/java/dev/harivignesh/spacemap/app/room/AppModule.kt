package dev.harivignesh.spacemap.app.room

import androidx.room.Room
import dev.harivignesh.spacemap.app.room.cache.AppDatabase
import dev.harivignesh.spacemap.app.room.network.ApiService
import dev.harivignesh.spacemap.app.room.presentation.RoomViewModel
import dev.harivignesh.spacemap.app.room.repository.RoomRepository
import dev.harivignesh.spacemap.app.room.repository.RoomRepositoryImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Hari on 26/09/2021.
 * Koin dependency
 */
val appModule = module {

    factory<ApiService> { ApiService.instance }

    single {
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "rooms")
            .build().roomDao()
    }

    factory<RoomRepository> { RoomRepositoryImpl(get(), get()) }

    viewModel { RoomViewModel(get()) }
}