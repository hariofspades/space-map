package dev.harivignesh.spacemap.app.room.model

/**
 * Created by Hari on 26/09/2021.
 * API models
 */
data class Room(val name: String, val spots: Int, val thumbnail: String)