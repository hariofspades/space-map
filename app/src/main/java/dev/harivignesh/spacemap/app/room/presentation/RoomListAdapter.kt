package dev.harivignesh.spacemap.app.room.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView
import dev.harivignesh.spacemap.app.R
import dev.harivignesh.spacemap.app.room.model.RoomEntity

/**
 * Created by Hari on 26/09/2021.
 *
 * Adapter for room list
 */
class RoomListAdapter(val onBookRoom: (room: RoomEntity) -> Unit) :
    ListAdapter<RoomEntity, RoomListAdapter.RoomViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_room, parent, false)
        return RoomViewHolder(layoutInflater)
    }

    override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class RoomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val title: MaterialTextView = itemView.findViewById(R.id.title)
        private val spots: MaterialTextView = itemView.findViewById(R.id.spots)
        private val thumbnail: ImageView = itemView.findViewById(R.id.thumbnail)
        private val bookNow: MaterialButton = itemView.findViewById(R.id.bookNow)

        fun bind(room: RoomEntity) {
            title.text = room.name

            val format = spots.context.getString(R.string.spots_remaining)
            spots.text = String.format(format, room.spots.toString())

            //TODO: Move the logic out
            bookNow.isEnabled = room.spots > 0
            bookNow.setOnClickListener { onBookRoom.invoke(room) }

            Glide.with(thumbnail.context).load(room.thumbnail).into(thumbnail)
        }
    }
}

private object DiffCallback : DiffUtil.ItemCallback<RoomEntity>() {
    override fun areItemsTheSame(oldItem: RoomEntity, newItem: RoomEntity): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: RoomEntity, newItem: RoomEntity): Boolean {
        return oldItem == newItem
    }

}