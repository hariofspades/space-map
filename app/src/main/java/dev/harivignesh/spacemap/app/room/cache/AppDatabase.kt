package dev.harivignesh.spacemap.app.room.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import dev.harivignesh.spacemap.app.room.model.RoomEntity

/**
 * Created by Hari on 26/09/2021.
 * App database
 */
@Database(entities = [RoomEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun roomDao(): RoomDao
}