package dev.harivignesh.spacemap.app.room.repository

import dev.harivignesh.spacemap.app.room.model.RoomEntity

/**
 * Created by Hari on 26/09/2021.
 * Repository - abstracts data sources
 */
interface RoomRepository {

    /**
     * get the list of rooms
     * @return list of [RoomEntity]
     */
    suspend fun getRooms(): List<RoomEntity>

    /**
     * book room, usually with ID (not sure in this case)
     * @return [Boolean]
     */
    suspend fun bookRoom(): Boolean
}