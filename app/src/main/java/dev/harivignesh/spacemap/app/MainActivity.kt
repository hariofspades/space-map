package dev.harivignesh.spacemap.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Hari on 26/09/2021.
 * Main activity
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}