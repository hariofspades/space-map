package dev.harivignesh.spacemap.app

import android.app.Application
import dev.harivignesh.spacemap.app.room.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Hari on 26/09/2021.
 * Application class
 */
class SpaceMapApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SpaceMapApp)
            modules(appModule)
        }
    }
}