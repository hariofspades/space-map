package dev.harivignesh.spacemap.app.room.repository

import dev.harivignesh.spacemap.app.room.cache.RoomDao
import dev.harivignesh.spacemap.app.room.model.RoomEntity
import dev.harivignesh.spacemap.app.room.network.ApiService
import java.lang.Exception

/**
 * Created by Hari on 26/09/2021.
 * Room repository implementation
 */
class RoomRepositoryImpl(
    private val api: ApiService,
    private val dao: RoomDao
) : RoomRepository {

    //TODO: Better no internet case handling
    override suspend fun getRooms(): List<RoomEntity> = try {
        val remoteResult = api.getRooms().rooms

        // assign ID based on index for the database
        val entities = remoteResult
            .mapIndexed { index, room -> room.mapToEntity(index) }

        dao.insertAll(entities)
        dao.getRooms()

    } catch (exception: Exception) {
        // load from cache when on no network or error
        dao.getRooms()
    }

    override suspend fun bookRoom(): Boolean = api.bookRoom().success
}