package dev.harivignesh.spacemap.app.room.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import dev.harivignesh.spacemap.app.R
import dev.harivignesh.spacemap.app.room.model.CallState
import kotlinx.android.synthetic.main.layout_scroll.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Hari on 26/09/2021.
 *``
 * Room list fragment
 */
//TODO: remove synthetic binding and use view binding
class RoomListFragment : Fragment(R.layout.fragment_room_list) {

    private val viewModel: RoomViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeRoomList()
        observeRoomBooking()
    }

    private fun observeRoomList() {
        viewModel.roomListState.observe(viewLifecycleOwner) { state ->
            when (state) {
                CallState.OnLoading -> progressBar.visibility = View.VISIBLE
                CallState.Empty -> {
                    progressBar.visibility = View.GONE
                    showMessage(getString(R.string.no_items_to_display), Snackbar.LENGTH_INDEFINITE)
                }
                is CallState.Error -> {
                    progressBar.visibility = View.GONE
                }
                is CallState.Success -> {
                    progressBar.visibility = View.GONE
                    roomListView.adapter =
                        RoomListAdapter { viewModel.bookRoom() }
                            .apply { submitList(state.data) }
                }
            }
        }
    }

    private fun observeRoomBooking() {
        viewModel.bookRoomState.observe(viewLifecycleOwner) { state ->
            when (state) {
                is CallState.Success ->
                    if (state.data) showMessage(getString(R.string.room_booked)) else
                        showMessage(getString(R.string.error))
                is CallState.Error -> showMessage(getString(R.string.error))
                else -> {
                    /** No implementation need yet **/
                }
            }
        }
    }

    private fun showMessage(message: String, duration: Int = Snackbar.LENGTH_LONG) =
        Snackbar.make(requireView(), message, duration).show()
}