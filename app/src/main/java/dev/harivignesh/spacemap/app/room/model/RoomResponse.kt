package dev.harivignesh.spacemap.app.room.model

/**
 * Created by Hari on 26/09/2021.
 * Response model for list of rooms API
 */
data class RoomResponse(val rooms: List<Room>)