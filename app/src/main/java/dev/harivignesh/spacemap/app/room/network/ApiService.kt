package dev.harivignesh.spacemap.app.room.network

import dev.harivignesh.spacemap.app.room.model.BookResponse
import dev.harivignesh.spacemap.app.room.model.RoomResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

/**
 * Created by hari on 26/09/2021.
 *
 * API endpoint definitions
 */
interface ApiService {

    @GET("rooms.json")
    suspend fun getRooms(): RoomResponse

    @GET("bookRoom.json")
    suspend fun bookRoom(): BookResponse

    companion object {
        val instance: ApiService = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://wetransfer.github.io/")
            .build()
            .create(ApiService::class.java)
    }
}