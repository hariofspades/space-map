package dev.harivignesh.spacemap.app.room.cache

import androidx.room.*
import dev.harivignesh.spacemap.app.room.model.RoomEntity

/**
 * Created by Hari on 26/09/2021.
 * Room dao object
 */
@Dao
interface RoomDao {

    @Query("SELECT * FROM roomentity")
    suspend fun getRooms(): List<RoomEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(rooms: List<RoomEntity>)

}