package dev.harivignesh.spacemap.app.room.model

/**
 * Created by Hari on 267/09/2021.
 * States for propagation
 */
sealed class CallState<T> {

    object OnLoading : CallState<Nothing>()

    object Empty : CallState<Nothing>()

    class Success<T : Any>(val data: T) : CallState<T>()

    class Error(val errorResponse: String?) : CallState<Nothing>()
}